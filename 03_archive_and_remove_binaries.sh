#!/bin/bash

#
# this script removes from the current HEAD all the way back 
# all the references to the files listed in the file passed 
# as a first argument. That file should be created with findBigFiles.sh
#
# All files 'removed' in this manner are 'archived'
#
# This script can be used to reduce the size of a git repo
#
# ATT: this script will rewrite all the histrory of the repo
# aka it is dangerous: BACKUP YOU REPO
#
# If you have unmerged branches this script will not work properly,
# it will leave the unmerged branched untouched and they will
# keep on referencing the data.
# 

# Check arugments
if [ -f "$1" ];then
    inputFile=$1;
else 
    echo "Error no arguments specified"
    echo "Usage: "
    echo "    $0 fileList "
    exit -1;
fi

# 
# check we are at the top directory
# 
if [ ! -d ".git" ]; then 
    echo "Error this command should be run from the top level directory of your git working directory"
    exit -1
fi

PWD=`pwd`
REPO_NAME=`basename $PWD`
ROLL_NAME=`grep ROLLNAME version.mk | awk '{print $3}'`
BINARIES_TO_REMOVE=$1
BINARY_HASHES=$2

echo "--------------------------------------------------------------------------------"
echo "git count-objects -v -- before files deletion"
git count-objects -v

echo "--------------------------------------------------------------------------------"
# Check for branches
echo "Checking for branches..."
git hist

echo "--------------------------------------------------------------------------------"
# Archive/remove binaries listed in BINARIES_TO_REMOVE
echo "Archiving & Removing binaries from the repository..."
for sha1 in `awk '{print $2}' $BINARIES_TO_REMOVE`
do
    # Archives binary bits from CVS repository
    BINARY_SRC=`grep $sha1 $BINARIES_TO_REMOVE | awk '{print $3}'`
    BINARY_SRC_NAME=`basename $BINARY_SRC`
    BINARY_SRC_DIR=`dirname $BINARY_SRC`

	echo "  Archiving $BINARY_SRC_NAME..."	
	../archive_blob.sh $ROLL_NAME $sha1 $BINARY_SRC
	
	echo "  Adding $BINARY_SRC_NAME to binary_hashes..."
	../hash_blob.sh $ROLL_NAME $sha1 $BINARY_SRC | tee -a ${BINARY_SRC_DIR}/${BINARY_HASHES}
    git add ${BINARY_SRC_DIR}/${BINARY_HASHES}
	git commit -m "Adding ${BINARY_SRC_NAME} to ${BINARY_HASHES}"
	
	echo "  Removing $BINARY_SRC_NAME..."	
	git obliterate $BINARY_SRC
done

echo "--------------------------------------------------------------------------------"
echo "Archiving and Removing 'current' binaries from the repository..."
for binary in `awk '{print $3}' $BINARIES_TO_REMOVE`
do
    if [ -f $binary ]; then
      echo "  Removing 'current' $BINARY_SRC_NAME..."	
	  git rm $binary
	fi
done
git commit -m "Removed current binaries."

echo "--------------------------------------------------------------------------------"
# Repack the repository...
echo "Repacking the repository..."
git repack -a -d --depth=250 --window=250

echo "--------------------------------------------------------------------------------"
echo git count-objects -v -- after files deletion
git count-objects -v


echo "--------------------------------------------------------------------------------"
echo "Archive and Removal complete."
echo ""
echo "Next step..."
echo ""
echo "cd ../ && ./04_clone_to_bare.sh ${REPO_NAME}"
