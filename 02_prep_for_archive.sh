#!/bin/bash

#
# This script prepares at Triton git archive for removal of binaries.
# 

# 
# check we are in a git working directory
# 
if [ ! -d ".git" ]; then 
    echo "Error this command should be run from a git working directory"
    exit -1
fi

PWD=`pwd`
REPO_NAME=`basename $PWD`
ROLL_NAME=`grep ROLLNAME version.mk | awk '{print $3}'`

echo "--------------------------------------------------------------------------------"
# Display 'current' binaries
echo "Displaying 'current' binaries (not ALL inclusive)..."
find . -type f | egrep "gz|zip|rpm"

echo "--------------------------------------------------------------------------------"
# Create removed_binaries file
echo "Generating 'removed_binaries' file..."
../findBigFile.sh 10240 | tee removed_binaries

echo "--------------------------------------------------------------------------------"
# Edit the removed_binaries file
read -p 'Press [ENTER] to edit the removed_binaries file... ' resp
vim ./removed_binaries

echo "--------------------------------------------------------------------------------"
# Committing removed_binaries file
git add removed_binaries
git commit -m "Binaries to remove enumerated."

echo "--------------------------------------------------------------------------------"
echo "Preparation complete."
echo ""
echo "Next step..."
echo ""
echo "../03_archive_and_remove_binaries.sh removed_binaries binary_hashes"