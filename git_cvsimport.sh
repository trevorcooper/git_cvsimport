#!/bin/bash
#
# See... 
#  http://stackoverflow.com/questions/11362676/ \
#    what-is-the-best-way-to-import-a-cvs-repository-in-git-and-use-it-locally
#
# Anon CVS from sourceforge...
#
#   for example... http://cc-mode.sourceforge.net/anoncvs.php
#
# example usage...
#
#  $ date && ./git_cvsimport.sh cc-mode && date
#  Mon Sep 16 15:56:51 PDT 2013
#  Logging in to 
#  :pserver:anonymous@cc-mode.cvs.sourceforge.net:2401/cvsroot/cc-mode
#  CVS password:
#  Mon Sep 16 16:46:04 PDT 2013


MODULENAME=$1
#USERNAME="anonymous"
USERNAME="${USER}"
#SERVER="${MODULENAME}.cvs.sourceforge.net"
SERVER="cvs.rocksclusters.org"
#CVSROOT=":pserver:${USERNAME}@${SERVER}:/cvsroot/${MODULENAME}"
CVS_RSH=ssh
CVSROOT=":ext:${USERNAME}@${SERVER}:/home/cvs/CVSROOT"
CVSPATH="triton/src/roll"

CWD=`pwd`
ERROR_LOG="${CWD}/${MODULENAME}_git_cvsimport.err"
OUTPUT_LOG="${CWD}/${MODULENAME}_git_cvsimport.out"
AUTHORS_FILE="${CWD}/${MODULENAME}_authors-file.txt"

# Create a destination...
mkdir -p "./${CVSPATH}/${MODULENAME}"

# Create/clear logfiles...
if [ -f "${ERROR_LOG}" ];
then
  cat /dev/null > "${ERROR_LOG}"
else
  touch  "${ERROR_LOG}"
fi
if [ -f "${OUTPUT_LOG}" ];
then
  cat /dev/null > "${OUTPUT_LOG}"
else
  touch  "${OUTPUT_LOG}"
fi

# anonymous login... press <ENTER>
#echo "Anonymous cvs login..." >> ${OUTPUT_LOG}
#cvs -d ${CVSROOT} login

# checkout latest branch to parse author list
echo "Removing existing module ${MODULENAME}..." | tee -a ${OUTPUT_LOG}
if [ -d ${CWD}/${CVSPATH}/${MODULENAME} ];
then
  rm -rf ${CWD}/${CVSPATH}/${MODULENAME}
fi
echo "Checkout latest branch..." | tee -a ${OUTPUT_LOG}
cvs -z3 -d ${CVSROOT} co ${CVSPATH}/${MODULENAME} 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}

# create authors-file.txt from CVS log
echo "Create ${AUTHORS_FILE}..." | tee -a ${OUTPUT_LOG}
pushd ./${CVSPATH}/${MODULENAME} 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
cvs -z3 -d ${CVSROOT} rlog ${CVSPATH}/${MODULENAME} 2>>${ERROR_LOG} | \
  grep "author: " | \
  cut -d\; -f2 | \
  sed 's,author: ,,g' | \
  sort | uniq | \
  awk '{print $1":"}' > ${AUTHORS_FILE}
popd 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
join authors.txt ${AUTHORS_FILE} > foo && mv foo ${AUTHORS_FILE}

# Convert to git
# git cvsimport -C target-cvs -r cvs -k -vA authors-file.txt -d $CVSROOT module
echo "Starting git cvsimport on `date`..." | tee -a ${OUTPUT_LOG}
git cvsimport -C ${CWD}/${MODULENAME} \
              -r cvs \
              -k -vA ${AUTHORS_FILE} \
              -d ${CVSROOT} \
              ${CVSPATH}/${MODULENAME} 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}

echo "Finished git cvsimport on `date`..." | tee -a ${OUTPUT_LOG}

exit
              
# Wipe out CVS directories
echo "Clear CVS directories..." >> ${OUTPUT_LOG}
pushd ${CWD}/${MODULENAME} 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
find . -type d -name CVS | xargs \rm -rf 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
popd 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}

# Replicate .cvsignore into .gitignore
pushd ${CWD}/${MODULENAME} 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
if [ -f .cvsignore ];
then
  echo "Create/commit .gitignore ..." >> ${OUTPUT_LOG}
  cp .cvsignore .gitignore
  git add .gitignore 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
  git commit -m "Added .gitignore" .gitignore 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
fi
popd 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}

# Get git status
echo "Get git status and history..." >> ${OUTPUT_LOG}
pushd ${CWD}/${MODULENAME} 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
git status  2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
git log --pretty=format:'%h %ad | %s%d [%an <%ae>]' --graph --date=short  2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
popd 2>>${ERROR_LOG} 1>>${OUTPUT_LOG}
