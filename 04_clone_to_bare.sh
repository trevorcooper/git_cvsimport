#!/bin/bash

#
# This script creates a 'clean & bare' clone of a converted and stripped
# Triton roll repo.
# 

# Check arugments
if [[ $1 ]];then
    REPO_NAME=$1;
else 
    echo "Error no arguments specified"
    echo "Usage: "
    echo "    $0 <repo_name> "
    exit -1;
fi

# 
# check we are NOT in a git working directory
# 
if [ -d ".git" ]; then 
    echo "Error this command should NOT be run from a git working directory"
    exit -1
fi

echo "--------------------------------------------------------------------------------"
# Create a 'clean & bare' clone...
echo "Cloning the repository..."
git clone --bare --no-hardlinks file://`pwd`/${REPO_NAME} ${REPO_NAME}-clone.git

echo "--------------------------------------------------------------------------------"
# Check for large files...
echo "Remaining large files in repository..."
../findBigFile.sh 10240

echo "--------------------------------------------------------------------------------"
# Rename repository
echo "Renaming repository clone..."
\rm -rf ./${REPO_NAME}
mv -v ./${REPO_NAME}-clone.git ${REPO_NAME}.git

echo "--------------------------------------------------------------------------------"
# Change remote url
echo "Changing remote url..."
cd ${REPO_NAME}.git
echo "git remote -v -- remote before change"
git remote -v

git remote set-url origin https://github.com/sdsc/${REPO_NAME}.git
echo "git remote -v -- remote after change"
git remote -v

echo "--------------------------------------------------------------------------------"
echo "Clone to clean & bare repository complete."
echo ""
echo "Next step..."
echo ""
echo "cd ${REPO_NAME}.git && ../05_create_repo_and_push.sh"