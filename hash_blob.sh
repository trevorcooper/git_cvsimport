#!/bin/bash
 
ARCHIVE_DIR=/Users/${USER}/cvs/git_cvsimport/archive

# Archives binary bits from CVS repository
ROLL_NAME=$1
SHA1=$2
BINARY_SRC_NAME=`basename $3`
BINARY_SRC_DIR=`dirname $3`

DEST_DIR=$ARCHIVE_DIR/$ROLL_NAME/$BINARY_SRC_DIR

ARCHIVED_OBJ=$DEST_DIR/$BINARY_SRC_NAME

if [ -f $ARCHIVED_OBJ ]; then

  OBJ_SIZE=`ls -l $DEST_DIR/$BINARY_SRC_NAME | awk '{print $5}'`
  OBJ_HASH=`git hash-object -t blob $DEST_DIR/$BINARY_SRC_NAME`
  OBJ_NAME=${BINARY_SRC_NAME}

  printf "%15d  %40s  %s\n" $OBJ_SIZE $OBJ_HASH $OBJ_NAME
fi
