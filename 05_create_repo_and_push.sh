#!/bin/bash
#

GIT_HOST=github.com
GIT_API_HOST=api.github.com
GIT_API_PORT=443

GIT_USER_NAME="Trevor Cooper"
GIT_USER_EMAIL=tcooper@sdsc.edu
GIT_USER_ACCOUNT=tcooper
GIT_ORG_ACCOUNT=sdsc

PWD=`pwd`
REPO_NAME=`basename $PWD`
REPO_IS_PRIVATE=true

# Create a private org repository
echo "--------------------------------------------------------------------------------"
echo "Creating a private=${REPO_IS_PRIVATE} repo for ${REPO_NAME} at https://github.com/${GIT_ORG_ACCOUNT}"
curl -u $GIT_USER_ACCOUNT -X POST -H 'Content-Type: application/x-www-form-urlencoded' -d '{"name": "'"$REPO_NAME"'","private": "'"$REPO_IS_PRIVATE"'"}' https://${GIT_API_HOST}:${GIT_API_PORT}/orgs/${GIT_ORG_ACCOUNT}/repos

# Push to private org repository
echo "--------------------------------------------------------------------------------"
echo "Pushing ${REPO_NAME} to GitHub..."
git push -u origin master
cd ../

echo "--------------------------------------------------------------------------------"
echo "Process complete for ${REPO_NAME}"
echo ""
echo "Next steps..."
echo ""
echo "cd ../ && mv -v ${REPO_NAME} ~/triton-git/ && \rm -rf ~/triton-git/"`echo ${REPO_NAME} | sed 's,\.git,,g'`
echo ""
echo "cd archive && scp -r "`echo ${REPO_NAME} | sed 's,-roll.git,,g'`"/ forge.sdsc.edu:/var/www/html/triton/ && cd ../"
echo ""
echo "Then continue migrating from the following list..."
echo ""
ls ~/triton-git/ | egrep -v "archive|git" | column -c 240 | column -t