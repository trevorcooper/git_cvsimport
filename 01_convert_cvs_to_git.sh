#!/bin/bash

#
# This script converts a Triton CVS repository to Git.
# 

# Check arugments
if [[ $1 ]];then
    CVS_REPO_NAME=$1;
else 
    echo "Error no arguments specified"
    echo "Usage: "
    echo "    $0 <cvs_repo_name> "
    exit -1;
fi

# 
# check we are NOT in a git working directory
# 
if [ -d ".git" ]; then 
    echo "Error this command should NOT be run from a git working directory"
    exit -1
fi

PWD=`pwd`
GIT_REPO_NAME=`echo $CVS_REPO_NAME | sed 's,-roll$,,g;s,$,-roll,g'`

echo "--------------------------------------------------------------------------------"
# Convert CVS repo to Git
echo "Converting CVS repo, ${CVS_REPO_NAME}, to git repo, ${GIT_REPO_NAME}..."
./git_cvsimport.sh ${CVS_REPO_NAME}

echo "--------------------------------------------------------------------------------"
# Display authors
echo "CVS repository authors to notify..."
cat ${CVS_REPO_NAME}_authors-file.txt

echo "--------------------------------------------------------------------------------"
# Change CVS repo directory name to SDSC Git repo name
echo "Changing repository name to SDSC standard and entering repository..."
mv -v ${CVS_REPO_NAME} ${GIT_REPO_NAME}

if [ -d ${GIT_REPO_NAME} ]; then
  cd ./${GIT_REPO_NAME}
else
  echo "${GIT_REPO_NAME} directory not found... exiting."
  exit
fi

echo "--------------------------------------------------------------------------------"
echo "Conversion complete."
echo ""
echo "Next steps..."
echo ""
echo 'egrep "[Ee]rror|[Ff]ail|[Ww]arn" '${CVS_REPO_NAME}'_git_cvsimport.*'
echo "cd ./${GIT_REPO_NAME} && ../02_prep_for_archive.sh"