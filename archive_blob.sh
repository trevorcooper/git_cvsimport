#!/bin/bash
 
ARCHIVE_DIR=/Users/${USER}/cvs/git_cvsimport/archive

# Archives binary bits from CVS repository
ROLL_NAME=$1
SHA1=$2
BINARY_SRC_NAME=`basename $3`
BINARY_SRC_DIR=`dirname $3`

DEST_DIR=$ARCHIVE_DIR/$ROLL_NAME/$BINARY_SRC_DIR

if ! [ -d $DEST_DIR ]; then
  mkdir -p $DEST_DIR
fi

git cat-file blob $SHA1 > $DEST_DIR/$BINARY_SRC_NAME
